<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WPSlider extends Model
{
    protected $table = "wpslider";
}

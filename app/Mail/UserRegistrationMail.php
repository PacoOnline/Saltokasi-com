<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\View\View;


class UserRegistrationMail extends Mailable
{
    use Queueable, SerializesModels;

    public  $kullanici;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $kullanici)
    {
        $this->$kullanici = $kullanici;
        //View::share('registratedUser', $this->registratedUser);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('info@saltokasi.com')
            ->subject(config('app.name'. ' - Kullanıcı Kaydı'))
            ->view('mails/registrationmail');
    }
}

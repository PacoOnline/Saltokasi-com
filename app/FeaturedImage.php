<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FeaturedImage extends Model
{
    public $table = "featured_images";
    protected $guarded = [];

}

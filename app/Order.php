<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
 protected $table = "orders";
  protected $guarded = [];


  public function sepet()
  {
      return $this->belongsTo('App\ShoppingCardProduct', 'sepet_id');
  }

  public function detail($id)
  {
      return view('orders');
  }

}

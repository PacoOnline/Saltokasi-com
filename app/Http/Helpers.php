<?php


class Helpers
{

    public static function diffForHumans($date)
    {
       return \Carbon\Carbon::parse($date)->diffForHumans();
    }


}

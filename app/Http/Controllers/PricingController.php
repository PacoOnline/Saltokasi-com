<?php


namespace App\Http\Controllers;
use App\CertainOrder;
use App\Order;
use App\ShoppingCard;
use App\ShoppingCardProduct;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Iyzipay\Model\Address;
use Iyzipay\Model\BasketItem;
use Iyzipay\Model\BasketItemType;
use Iyzipay\Model\Buyer;
use Iyzipay\Model\CheckoutFormInitialize;
use Iyzipay\Model\Currency;
use Iyzipay\Model\Locale;
use Iyzipay\Model\PaymentGroup;
use Iyzipay\Request\RetrieveCheckoutFormRequest;
use Iyzipay\Options;
use Iyzipay\Request;
use Iyzipay\Request\CreateCheckoutFormInitializeRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use League\CommonMark\Input;
use Illuminate\Http\Request as LaravelRequest;
use Prophecy\Argument\Token\CallbackToken;
use Ramsey\Uuid\Generator\RandomBytesGenerator;
use Symfony\Component\Console\Input as LaravelInput;
use Illuminate\Support\Str;

class PricingController extends Controller
{
    public function index(LaravelRequest $newRequest)
    {


        $options = new Options();
        $options->setApiKey('DvzSctJwDFWDrCCRJD6MtrSeYyfMKgTH');
        $options->setSecretKey('fIuzYV8aQyga9lyUeaXBlS2HB4ZyPVZi');
        $options->setBaseUrl('https://api.iyzipay.com');


        $conversationId = Str::random(9);

        $request = new CreateCheckoutFormInitializeRequest();
        $request->setLocale(Locale::TR);
        $request->setConversationId($conversationId);
        $request->setPrice(1);
        $request->setPaidPrice(Cart::total());
        $request->setCurrency(Currency::TL);
        $request->setBasketId("B67832");
        $request->setPaymentGroup(PaymentGroup::PRODUCT);
        $request->setCallbackUrl("https://saltokasi.com/odemesonuc");
        $request->setEnabledInstallments(array(2, 3, 6, 9));

        $buyerId = Str::random(4);

        $buyer = new Buyer();
        $buyer->setId($buyerId);
        $buyer->setName($newRequest->Input('name'));
        $buyer->setSurname($newRequest->Input('surname'));
        $buyer->setGsmNumber($newRequest->Input('phonenumber'));
        $buyer->setEmail($newRequest->Input('email'));
        $buyer->setIdentityNumber($newRequest->Input('identitynumber'));
        $buyer->setLastLoginDate(date("Y-m-d h:i:sa"));
        $buyer->setRegistrationDate(date("Y-m-d h:i:sa"));
        $buyer->setRegistrationAddress($newRequest->Input('address'));
        $buyer->setIp($_SERVER['REMOTE_ADDR']);
        $buyer->setCity($newRequest->Input('city'));
        $buyer->setCountry("Turkey");
        $buyer->setZipCode("34732");
        $request->setBuyer($buyer);

        $shippingAddress = new Address();
        $shippingAddress->setContactName($newRequest->Input('name'));
        $shippingAddress->setCity($newRequest->Input('city'));
        $shippingAddress->setCountry("Turkey");
        $shippingAddress->setAddress($newRequest->Input('address'));
        $shippingAddress->setZipCode("34000");
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new Address();
        $billingAddress->setContactName($newRequest->Input('name').($newRequest->Input('surname')));
        $billingAddress->setCity($newRequest->Input('city'));
        $billingAddress->setCountry("Turkey");
        $billingAddress->setAddress($newRequest->Input('address'));
        $billingAddress->setZipCode("34000");
        $request->setBillingAddress($billingAddress);


        $basketItems = array();
        $firstBasketItem = new BasketItem();
        $firstBasketItem->setId("BI0101");
        $firstBasketItem->setName("Binocular");
        $firstBasketItem->setCategory1("Collectibles");
        $firstBasketItem->setCategory2("Accessories");
        $firstBasketItem->setItemType(BasketItemType::PHYSICAL);
        $firstBasketItem->setPrice("0.3");
        $basketItems[0] = $firstBasketItem;

        $secondBasketItem = new BasketItem();
        $secondBasketItem->setId("BI102");
        $secondBasketItem->setName("Game code");
        $secondBasketItem->setCategory1("Game");
        $secondBasketItem->setCategory2("Online Game Items");
        $secondBasketItem->setItemType(BasketItemType::VIRTUAL);
        $secondBasketItem->setPrice("0.5");
        $basketItems[1] = $secondBasketItem;

        $thirdBasketItem = new BasketItem();
        $thirdBasketItem->setId("BI103");
        $thirdBasketItem->setName("Usb");
        $thirdBasketItem->setCategory1("Electronics");
        $thirdBasketItem->setCategory2("Usb / Cable");
        $thirdBasketItem->setItemType(BasketItemType::PHYSICAL);
        $thirdBasketItem->setPrice("0.2");
        $basketItems[2] = $thirdBasketItem;
        $request->setBasketItems($basketItems);
        $request->setBasketItems($basketItems);

        $checkoutFormInitialize = CheckoutFormInitialize::create($request, $options);
        $paymentinput = $checkoutFormInitialize->getCheckoutFormContent();
        $basketItems = array();

        $checkoutFormInitialize = CheckoutFormInitialize::create($request, $options);
        $paymentinput = $checkoutFormInitialize->getCheckoutFormContent();
        $user_detail = auth()->user();

        $getlastcard = \DB::table('shopping_card')
            ->orderBy('id', 'desc')
            ->where('user_id',  auth()->id())
            ->limit('1')
            ->get();

        $createorder = Order::create([
            'name'=> $newRequest->Input('name'),
            'surname'=> $newRequest->Input('surname'),
            'city'=> $newRequest->Input('city'),
            'address'=> $newRequest->Input('address'),
            'email'=> $newRequest->Input('email'),
            'phonenumber'=> $newRequest->Input('phonenumber'),
            'identitynumber'=> $newRequest->Input('identitynumber'),
            'notes'=> $newRequest->Input('notes'),
            'sepet_id'=> $getlastcard[0]->id,
            'price'=> Cart::total(),
            'user_id'=> auth()->id(),
            'ip_address'=> $newRequest->Input('ip_address'),
            'status'=> $newRequest->Input('status', 'Sipariş alınmıştır'),
        ]);

        $newRequest->session()->put('usersid' ,auth()->id());

        print_r($checkoutFormInitialize->getStatus());
        print_r($checkoutFormInitialize->getErrorMessage());
        print_r($checkoutFormInitialize->getCheckoutFormContent());

         //$session_data = session('active_card_id');
         //Cart::destroy();
         //session()->forget('active_card_id');

        return view('iyzicopaymentform' ,compact('paymentinput'));
    }

    public function senttokendata(LaravelRequest $laravelRequest)
    {
        $options = new Options();
        $options->setApiKey('DvzSctJwDFWDrCCRJD6MtrSeYyfMKgTH');
        $options->setSecretKey('fIuzYV8aQyga9lyUeaXBlS2HB4ZyPVZi');
        $options->setBaseUrl('https://api.iyzipay.com');

        $laravelRequest->session();
        $token = $laravelRequest->input('token');
        $request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setToken($_POST["token"]);
        $checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($request, $options);
        $payment_status = $checkoutForm->getPaymentStatus();
        $getlastorder = \DB::table('orders')
             ->orderBy('id', 'desc')
             ->where('ip_address', $_SERVER['REMOTE_ADDR'])
             ->limit('1')
             ->get();
        echo "<br>";
          if ($payment_status == "FAILURE")
          {
            echo "ödeme başarısız";
          }
        elseif($payment_status == "SUCCESS")
        {
            $createcertainorder = CertainOrder::create([
                'name'=> $getlastorder[0]->name,
                'surname'=> $getlastorder[0]->surname,
                'city'=> $getlastorder[0]->city,
                'address'=> $getlastorder[0]->address,
                'email'=> $getlastorder[0]->email,
                'phonenumber'=> $getlastorder[0]->phonenumber,
                'identitynumber'=> $getlastorder[0]->identitynumber,
                'notes'=> $getlastorder[0]->notes,
                'sepet_id'=> $getlastorder[0]->sepet_id,
                'price'=> $getlastorder[0]->price,
                'user_id'=> $getlastorder[0]->user_id,
                'status'=> $getlastorder[0]->status,
            ]);
            echo "ödeme başarılı";
            Cart::destroy();
            session()->forget('active_card_id');

        }

      }

}


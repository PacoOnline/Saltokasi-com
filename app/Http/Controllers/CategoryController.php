<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{

    public function CategoryDetails($id)
    {
      
      $CategoryDetails  = Category::where('slug' , $id)->first();
       return view('categories', compact ('CategoryDetails'));
    }

}
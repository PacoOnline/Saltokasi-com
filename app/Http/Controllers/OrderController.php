<?php

namespace App\Http\Controllers;

use App\Order;
use App\CertainOrder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
   public function index()
   {
       if (!auth()->check())
       {
           return redirect()->route('kullanici.giris-yap');
       }
       $orders =  \DB::table('certain_orders')
           ->orderBy('id', 'desc')
           ->where('user_id',  auth()->id())
           //->limit('1')
           ->get();
       return view('orders', compact('orders'));
   }
}

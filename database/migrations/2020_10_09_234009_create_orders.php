<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 20);
            $table->string('surname', 20);
            $table->string('city',10);
            $table->string('address',400);
            $table->string('email');
            $table->string('phonenumber');
            $table->string('identitynumber');
            $table->string('notes')->nullable();
            $table->integer('sepet_id')->unsigned();
            $table->decimal('price', 5,4);
            $table->string('token')->nullable();
            $table->string('status', 50)->nullable();
            $table->integer('user_id')->nullable();
            $table->string('ip_address');

            $table->foreign('sepet_id')->references('id')->on('shopping_card_product')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

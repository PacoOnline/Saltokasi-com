<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_images', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('image_left');
            $table->string('imgleft_product_slug')->nullable();
            $table->string('title_left')->nullable();
            $table->string('image_right');
            $table->string('imgright_product_slug')->nullable();
            $table->string('title_right')->nullable();
            $table->string('long_image');
            $table->string('title_long_image')->nullable();
            $table->string('imglong_product_slug')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_images');
    }
}

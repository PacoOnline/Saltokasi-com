<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-179398468-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fazi Şal tokası</title>

    <link href="/img/fazifavicon2.ico" rel="shortcut icon"/>


    <!-- Bootstrap Core CSS -->
    <link href="/cleanblog/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="/cleanblog/css/clean-blog.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/cleanblog/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Şal Tokası</title>

<!-- Icon css link -->
<link href="/css/font-awesome.min.css" rel="stylesheet">
<link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
<link href="/vendors/elegant-icon/style.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="/css/bootstrap.min.css" rel="stylesheet">

<!-- Rev slider css -->
<link href="/vendors/revolution/css/settings.css" rel="stylesheet">
<link href="/vendors/revolution/css/layers.css" rel="stylesheet">
<link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

<!-- Extra plugin css -->
<link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
<link href="/vendors/vertical-slider/css/jQuery.verticalCarousel.css" rel="stylesheet">

<link href="/css/style.css" rel="stylesheet">
<link href="/css/responsive.css" rel="stylesheet">
<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/4.12/video.js"></script>
@include('partials.navforblog')
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->

<header class="intro-header" style="background-image: url('img/categories_image.jpeg')">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1><a style="color: white" >Fazi Şal tokası nasıl kullanılır?</a></h1>
                    <span class="meta"><a style="color: white" > Eylül 25, 2020, Fazilet Şengül</a></span>
                </div>
            </div>
        </div>
    </div>
</header>
<style>

    .adobewordpress{
        width: 320px;
        height: 180px;
        position: absolute;

        /* Oynatıcının Arka Plan Ayarları*/
        background: #092756;
        background: -moz-radial-gradient(0% 100%, ellipse cover, rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%),-moz-linear-gradient(top, rgba(57,173,219,.25) 0%, rgba(42,60,87,.4) 100%), -moz-linear-gradient(-45deg, #670d10 0%, #092756 100%);
        background: -webkit-radial-gradient(0% 100%, ellipse cover, rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%), -webkit-linear-gradient(top, rgba(57,173,219,.25) 0%,rgba(42,60,87,.4) 100%), -webkit-linear-gradient(-45deg, #670d10 0%,#092756 100%);
        background: -o-radial-gradient(0% 100%, ellipse cover, rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%), -o-linear-gradient(top, rgba(57,173,219,.25) 0%,rgba(42,60,87,.4) 100%), -o-linear-gradient(-45deg, #670d10 0%,#092756 100%);
        background: -ms-radial-gradient(0% 100%, ellipse cover, rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%), -ms-linear-gradient(top, rgba(57,173,219,.25) 0%,rgba(42,60,87,.4) 100%), -ms-linear-gradient(-45deg, #670d10 0%,#092756 100%);
        background: -webkit-radial-gradient(0% 100%, ellipse cover, rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%), linear-gradient(to bottom, rgba(57,173,219,.25) 0%,rgba(42,60,87,.4) 100%), linear-gradient(135deg, #670d10 0%,#092756 100%);
        padding:15px;
        border-radius:10px;

        /* Oynatıcının Gölge Ayarları*/
        -webkit-box-shadow: 0 15px 10px -10px rgba(0, 0, 0, 0.5), 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
        -moz-box-shadow: 0 15px 10px -10px rgba(0, 0, 0, 0.5), 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
        box-shadow: 0 15px 10px -10px rgba(0, 0, 0, 0.5), 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }

    .kanal{
        /* Kanal Logosu */
        position: absolute;
        margin:30px;
        width:70px;
        left:0;
        top:0;
        z-index:999;
    }
    .altyazi{
        position:relative;
        bottom:60px;
        text-align:center;
        width: 95%;
        margin:0 auto;
        font:bold 12px Helvetica;
        color:white;
        text-shadow: 2px 2px 1px #000;
        padding:10px;
    }

    #metinler{display:none; }
</style>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p>Kullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin.
                    Diğer şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip stoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza şekil verin ve rahatlığa adım atın!</p>
                <video width=100% height="300px" controls>

                    <!--MP4 tipi video dosyası için-->
                    <source src="/img/active_video.mp4"  type="video/mp4">

                </video>
                <video width=100% height="300px" controls>

                    <!--MP4 tipi video dosyası için-->
                    <source src="/img/secondvideo.mp4"  type="video/mp4">

                </video>
            </div>

            </div>
        </div>


    </div>
</article>


<hr>

<!-- Footer -->
@include('partials.footer')
<!-- jQuery -->
<script src="/cleanblog/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/cleanblog/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/cleanblog/js/jqBootstrapValidation.js"></script>
<script src="/cleanblog/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="/cleanblog/js/clean-blog.min.js"></script>
<!-- Extra plugin css -->
<script src="vendors/counterup/jquery.waypoints.min.js"></script>
<script src="vendors/counterup/jquery.counterup.min.js"></script>
<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="vendors/isotope/isotope.pkgd.min.js"></script>
<script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="vendors/jquery-ui/jquery-ui.js"></script>
<script src="js/theme.js"></script>

</body>

</html>

   <!--================Footer Area =================-->
   <script>
   $(function() {
   if(IsMediaType('screen') > 0 && parseInt(screen.width) < 599) {
   $.getSscript("js/menu-collapser.js");
   }
   });

   </script>
   <footer class="footer_area border_none">
                <div class="container">
                    <div class="footer_widgets">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-6">
                                <aside class="f_widget f_about_widget">
                                    <img width="200px" src="/img/fazilogo.jpeg" alt="">
                                    <p>Fazi Şal tokası  tamamen el yapımıdır </p>
                                    <h6>Sosyal Medya:</h6>
                                    <ul>
                                        <li><a href="https://www.facebook.com/faziaksesuar"><i class="social_facebook"></i></a></li>
                                        <li><a href="https://www.instagram.com/fazitasarim/"><i class="social_instagram"></i></a></li>
                                        <li><a href="https://www.youtube.com/channel/UCc_hzvog7155XsmOO3s_hdg"><i class="social_youtube"></i></a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=905386293141&text=&source=&data=&app_absent=" ><i class="social_skype_circle"></i></a></li>

                                    </ul>

                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-4 col-6">
                                <aside class="f_widget link_widget f_info_widget">
                                    <div class="f_w_title">
                                        <h3>Bilgi</h3>
                                    </div>
                                    <ul>
                                        <li><a href="/hakkimizda">Hakkımızda</a></li>
                                        <li><a href="/mesafeli-satis-politikasi">Mesafeli Satış Sözleşmesi</a></li>
                                        <li><a href="/iptal-iade-kosullari">İade ve İptal koşulları</a></li>
                                        <li><a href="/gizlilik-politikası">Gizlilik Politikası</a></li>
                                    </ul>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-4 col-6">
                                <aside class="f_widget link_widget f_service_widget">
                                    <div class="f_w_title">
                                        <h3>Müşteri Hizmetleri</h3>
                                    </div>
                                    <ul>
                                        <li><a href="/iletisim">İletişim</a></li>
                                       <li><a href="#">Abone Bülteni</a></li>

                                    </ul>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-4 col-6">
                                <aside class="f_widget link_widget f_account_widget">
                                    <div class="f_w_title">
                                        <h3>Hesabım</h3>
                                    </div>
                                    <ul>
                                        <li><a href="/kullanici/hesabim">Hesabım</a></li>
                                        <li><a href="/siparis">Siparişlerim</a></li>
                                        <li><a href="#">İstek Listesi</a></li>
                                    </ul>
                                </aside>
                            </div>
                            <div class="col-lg-2 col-md-4 col-6">
                                <aside class="f_widget f_about_widget">
                                    <img width="150px" src="/img/logos.png" alt="">
                                    <img width="150px" src="/img/iyzicologo.png" alt="">

                                </aside>
                            </div>

                        </div>
                    </div>
                    <div class="footer_copyright">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="tel:05386293141" target="_blank">Umut Ozan Özyıldırım</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </h5>
                    </div>
                </div>
            </footer>
            <!--================End Footer Area =================-->
   <script src="/js/theme.js"></script>



<link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Şal Tokası</title>

<!-- Icon css link -->
<link href="/css/font-awesome.min.css" rel="stylesheet">
<link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
<link href="/vendors/elegant-icon/style.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="/css/bootstrap.min.css" rel="stylesheet">

<!-- Rev slider css -->
<link href="/vendors/revolution/css/settings.css" rel="stylesheet">
<link href="/vendors/revolution/css/layers.css" rel="stylesheet">
<link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

<!-- Extra plugin css -->
<link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
<link href="/vendors/vertical-slider/css/jQuery.verticalCarousel.css" rel="stylesheet">

<link href="/css/style.css" rel="stylesheet">
<link href="/css/responsive.css" rel="stylesheet">
<style>
    @media (max-width: 759px){
        /* your CSS here */
        #respoumut {
            alignment: left;
            height: 55px;
            width: auto;
        }
    }
</style>
<header class="shop_header_area carousel_menu_area">
    <div class="carousel_top_header black_top_header row m0">
        <div class="container">
            <div class="carousel_top_h_inner">
                <div class="float-md-left">
                    <div class="top_header_left">
                        <div class="selector">
                            <select class="language_drop" name="countries" id="countries" style="
                                        width:300px;">
                                <option value='yt' data-image="/img/icon/flag-3.png" data-imagecss="flag yt" data-title="Turkish">Türkçe</option>
                                <option value='yu' data-image="/img/icon/flag-1.png" data-imagecss="flag yu" data-title="Bangladesh">English</option>
                                <option value='yt' data-image="/img/icon/flag-2.png" data-imagecss="flag yt" data-title="Arabic">عربى</option>

                            </select>
                        </div>
                        <select class="selectpicker usd_select">
                            <option>TRY</option>
                            <option>USD</option>

                        </select>
                    </div>
                </div>
                <div class="float-md-right">
                    <ul class="account_list">
                        @guest
                            <li><a href="{{ route('kullanici.giris-yap') }}">Giriş yap</a></li>
                            <li><a href="{{ route('kullanici.kaydol') }}">Kayıt ol</a></li>
                        @endguest
                        @auth
                            <li><a href="/kullanici/hesabim">Hesabım</a></li>
                            <li><a href="#">İstek listesi</a></li>
                            <li><a href="#">Siparişlerim</a></li>
                            <li><a href="#"  onclick="event.preventDefault(); document.getElementById('logout-form').submit()" >Çıkış Yap</a></li>
                            <form id="logout-form" action="{{ route('kullanici.oturumukapat') }}" method="post" style="display: none">
                                @csrf
                            </form>
                        @endauth
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="carousel_menu_inner">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/"><img style="margin-left: 18pt" height="75px" width="80px" id="respoumut" src="/img/fazilogo.jpeg" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-5 mr-auto">
                        <li class="nav-item dropdown submenu active">
                            <a class="nav-item" href="{{ route('anasayfa') }}" role="button">
                                Anasayfa

                            </a>
                        </li>

                        <li class="nav-item dropdown submenu">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ürünler <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($CategoryLists as $CategoryList)
                                    <li><a href="/kategori/{{$CategoryList->slug}}">{{$CategoryList->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="/nasil-kullanilir">Nasıl Kullanılır?</a></li>

                        <li class="nav-item"><a class="nav-link" href="/iletisim">İLETİŞİM</a></li>
                    </ul>


                    <ul class="navbar-nav justify-content-end">
                        <li class="search_icon"><a href="#"><i class="icon-magnifier icons"></i></a></li>

                        <li class="user_icon"><a href="#"><i class="icon-user icons"></i></a></li>
                        <li class="cart_cart"><a href="/sepet"><i class="icon-handbag icons"></i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

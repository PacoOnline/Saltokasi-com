@if(count($errors)>0)
    @foreach($errors->all() as $error)

        <section class="shopping_cart_area mt-5">

            <div class="container">
                <div class="col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <li>{{$error}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @endforeach
@endif

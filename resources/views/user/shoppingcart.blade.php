<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179398468-1');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Sepetim</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
        <link href="vendors/elegant-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!--================Menu Area =================-->
        <!--================End Menu Area =================-->
     @include('partials.nav')
        <!--================Categories Banner Area =================-->
        <section class="solid_banner_area">
            <div class="container">
                <div class="solid_banner_inner">
                    <h3>Alışveriş Sepetim</h3>
                    <ul>
                        <li><a href="/">Anasayfa</a></li>
                        <li><a>Alışveriş Sepeti</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!--================End Categories Banner Area =================-->
         @include('partials.errors')
         @if (session()->has('success'))
            <section class="shopping_cart_area mt-5">

            <div class="container">
                    <div class="col-lg-12">
                        <div class="alert alert-success" role="alert">
                            Ürününüz Sepete Başarıyla Eklendi
                        </div>                    </div>
            </div>
            </section>


        @endif
        <!--================Shopping Cart Area =================-->
        @if(count(Cart::content())>0)
        <section class="shopping_cart_area p_100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="cart_items">
                            <h3>Sepetinizdeki Ürünler</h3>
                            <div class="table-responsive-md">
                                <table class="table">
                                    <tbody>
                                        @foreach(Cart::content() as $urunCartItem)
                                        <tr>
                                            <th scope="row">
                                              <form action="{{route('sepet.kaldir', $urunCartItem->rowId)}}" method="post" >
                                               @csrf
                                                  {{method_field('DELETE')}}

                                                  <input type="image" src="img/icon/close-icon.png" value="Sepetten kaldır">
                                              </form>

                                            </th>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img href="/urun/{{$urunCartItem->slug}}" height="128px" width="128px" src="/storage/{{ $urunCartItem->options->cover_photo }}" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <a style="color: #0b0b0b" href="/urun/{{$urunCartItem->slug}}">{{ $urunCartItem->name }}</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p class="red">{{ $urunCartItem->price}}  ₺</p></td>
                                            <td>
                                                <div class="quantity">
                                                    <h6>Miktar</h6>
                                                    <div class="custom">
                                                        <a href="#" class="btn btn-xs btn-default urun-adet-azalt">-</a>
                                                        <span>{{ $urunCartItem->qty}}</span>
                                                        <a href="#" class="btn btn-xs btn-default urun-adet-artir">+</a>
                                                     </div>
                                                </div>

                                            </td>

                                            <td><p>{{ $urunCartItem->subtotal }}  ₺</p></td>
                                        </tr>
                                        @endforeach
                                        <tr class="last">
                                            <th scope="row">
                                                <img src="img/icon/cart-icon.png" alt="">
                                            </th>
                                            <td>
                                        <div class="media">
                                        <div class="d-flex">
                                            <h5>Kupon Kodunuz</h5>
                                            </div>
                                            <div class="media-body">
                                                        <input type="text" placeholder="Apply coupon">


                                                    </div>
                                                </div>
                                            </td>
                                            <td><p class="red"></p></td>
                                            <td>
                                                <form method="post" action="{{ route('sepet.bosalt') }}">
                                                @csrf
                                                    {{ method_field('DELETE') }}
                                                    <input class="btn btn-danger" type="submit" value="Tüm Sepeti boşalt">
                                                </form>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="cart_totals_area">
                            <h4>Ödenecek Tutar</h4>
                            <div class="cart_t_list">
                                <div class="media">
                                    <div class="d-flex">
                                        <h5>Ürün tutarları</h5>
                                    </div>
                                <div class="media-body">
                                    <h6> &nbsp; {{ Cart::subtotal() }} ₺</h6>
                                </div>
                                </div>

                                <div class="media">
                                    <div class="d-flex">
                                        <div class="d-flex">
                                            <h5>KDV</h5>
                                        </div>
                                        <div class="media-body">
                                            <p>{{ Cart::tax() }}₺</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="total_amount row m0 row_disable">
                                <div class="float-left">
                                    Toplam
                                </div>
                                <div class="float-right">
                                    {{Cart::total()}}  ₺
                                </div>
                            </div>
                        </div>
                        <button type="submit" onclick="window.location.href='/odeme'" value="submit" class="btn subs_btn form-control">Ödemeye devam et</button>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Shopping Cart Area =================-->
        @else
            <section class="emty_cart_area p_100">
                <div class="container">
                    <div class="emty_cart_inner">
                        <i class="icon-handbag icons"></i>
                        <h3>Sepetiniz boş</h3>
                        <h4>Alışverişe <a href="{{ route('anasayfa') }}">Geri Dön</a></h4>
                    </div>
                </div>
            </section>
        @endif
        <!--================Footer Area =================-->
       @include('partials.footer')
        <!--================End Footer Area =================-->



        <section>
            <script>


            $('.urun-adet-artir, .urun-adet-azalt').on('click', function ()
            {
            var id = $(this).attr('data-id');
            var adet = $(this).attr('data-adet');

            $.ajax({
            type: 'PATCH',
            url: 'sepet/guncelle/' + id,
            data: {adet: adet},
            success: function (result) {
            window.location.href = '/sepet';
            }

            })
            }
            )
            </script>
        </section>



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="/vendors/counterup/jquery.counterup.min.js"></script>
        <script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
        <script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
        <script src="/js/smoothscroll.js"></script>
        <script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
        <script src="/vendors/jquery-ui/jquery-ui.js"></script>

        <script src="/js/theme.js"></script>
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-179398468-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/img/fazifavicon2.ico" rel="shortcut icon"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Şal Tokası</title>

    <!-- Icon css link -->
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
    <link href="/vendors/elegant-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="/vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/vendors/jquery-ui/jquery-ui.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--================Menu Area =================-->
@include('partials.nav')
<!--================End Menu Area =================-->

<!--================Categories Banner Area =================-->
<section class="solid_banner_area">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>Kayıt Ol</h3>
            <ul>
                <li><a>Anasayfa</a></li>
                <li><a >Kayıt Ol</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Categories Banner Area =================-->

<!--================login Area =================-->

<section class="login_area  p_100">
    <div class="container">
        <div class="login_inner">
            <div class="row">
                <div class="col-lg-12">
                    <div class="login_title">
                        <h2>Kullanıcı Oluşturun</h2>
                        <p>Fırsatlardan yararlanmak için ücretsiz bir kullanıcı hesabı oluşturun.</p>
                    </div>
                    <div>
                    </div>
                    <form role="form" method="POST" action="{{ route('kullanici.kaydol') }}" class="login_form row">
                      @csrf
                        <div class="col-lg-6 form-group">
                            <input id="name" class="form-control" type="name" name="name" placeholder="İSMİNİZ">
                            @if($errors->has('name'))
                             <span class="help-block">
                                 <strong>{{ $errors->first('name')}}</strong>
                             </span>
                            @endif
                        </div>
                        <div class="col-lg-6 form-group">
                            <input id="surname" class="form-control" type="surname" name="surname" placeholder="SOYİSMİNİZ">
                            @if($errors->has('surname'))
                                <span class="help-block">
                                 <strong>{{ $errors->first('surname')}}</strong>
                             </span>
                            @endif
                        </div>
                        <div class="col-lg-6 form-group">
                            <input id="email" class="form-control" type="email" name="email" placeholder="E-MAIL ADRESİNİZ">
                            @if($errors->has('email'))
                                <span class="help-block">
                                 <strong>{{ $errors->first('email')}}</strong>
                             </span>
                            @endif
                        </div>
                        <div class="col-lg-6 form-group">
                            <input id="phone_number" class="form-control" type="phone_number" name="phone_number" placeholder="TELEFON NUMARANIZ(BAŞINDA SIFIR OLACAK ŞEKİLDE)">
                            @if($errors->has('phone_number'))
                                <span class="help-block">
                                 <strong>{{ $errors->first('phone_number')}}</strong>
                             </span>
                            @endif
                        </div>
                        <div class="col-lg-6 form-group">
                            <input  id="password" class="form-control" type="password" name="password" placeholder="ŞİFRENİZ">
                            @if($errors->has('password'))
                                <span class="help-block">
                                 <strong>{{ $errors->first('password')}}</strong>
                             </span>
                            @endif
                        </div>
                        <div class="col-lg-6 form-group">
                            <input id="re-password" class="form-control" name="password_confirmation" type="password" placeholder="ŞİFRE TEKRAR">
                        </div>
                        <div class="col-lg-12 form-group">
                            <button type="submit" value="submit" class="btn subs_btn form-control">Şimdi Kayıt ol</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!--================End login Area =================-->

<!--================Footer Area =================-->
@include('partials.footer')
<!--================End Footer Area =================-->




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery-3.2.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- Rev slider js -->
<script src="/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<!-- Extra plugin css -->
<script src="/vendors/counterup/jquery.waypoints.min.js"></script>
<script src="/vendors/counterup/jquery.counterup.min.js"></script>
<script src="/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="/vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="/vendors/isotope/isotope.pkgd.min.js"></script>
<script src="/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="/vendors/jquery-ui/jquery-ui.js"></script>

<script src="/js/theme.js"></script>
</body>
</html>

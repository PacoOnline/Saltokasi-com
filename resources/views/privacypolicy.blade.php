<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179398468-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-179398468-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fazi Şal tokası</title>

    <link href="/img/fazifavicon2.ico" rel="shortcut icon"/>


    <!-- Bootstrap Core CSS -->
    <link href="/cleanblog/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="/cleanblog/css/clean-blog.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/cleanblog/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
@include('partials.navforblog')
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/gray-background.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1><a style="color: black" >Gizlilik Politikası</a></h1>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p>Şal Tokası E-Ticaret sitesi  Gizlilik İlkeleri Metni
                    [www.saltokasi.com] Sitesi Gizlilik İlkeleridir

                    Bu gizlilik ilkeleri Fazi tasarım & aksesuar  tarafından, Fazi tasarım & aksesuar şirketinin gizlilik konusundaki sorumluluklarının tesbiti için hazırlanmıştır. Aşağıdaki maddeler [site adresi] web sitesi üzerindeki bilgi toplama ve dağıtımı işlemlerinin kurallarını içermektedir.

                    IP adresinizi sunucularımızdaki sorunların giderilmesi ve İnternet sitemizi yönetmek için kullanacağız. IP adresiniz, sizi ve alışveriş sepetinizi tanımak ve açık demografik bilgilerinizin toplanması için kullanılacaktır.

                    Sitemizde alışveriş sepetinizin takibi ve aynı reklamların ardarda görülmesinin engellenmesi için Cookie’lerden yararlanılmaktadır. Cookie’lerden size ilgi alanlarınız doğrultusunda içerik sunulması ve tekrar tekrar şifre girmemeniz için şifrenizin saklanması gibi konularda yararlanılmaktadır.

                    Fazi tasarım & aksesuar şirketinin, [www.saltokasi.com] adresinde yer alan bağlantıların gizlilik politikaları ve içeriklerinden sorumlu değildir.

                    Sitede bir kayıt formu bulunuyorsa aşağıdaki metinden yararlanılabilir;
                    Sitemizin kayıt formunda kullanıcılarımız, iletişim bilgilerini (isim, adres, telefon, e-posta adresi ……gibi), finansal bilgilerini (kredi kartı bilgileri, hesap numarası) ve kişiye özel bilgilerini (vergi numarası, kimlik numarası…. vb.) girmelidir.

                    Bu formda aldığımız iletişim bilgilerini kullanıcılarımıza, firmamız ve tarafımızca belirlenen firmalar hakkında bilgi ve kampanya haberleri ve materyallerini göndermek için kullanmaktayız. İletişim bilgileri ayrıca kullanıcılarımızla iletişime geçmemiz gerektiğinde kullanılacak ve kullanıcımızla iletişime geçmek isteyen diğer firmalarla paylaşılacaktır.

                    Kullanıcılarımız isteklerine bağlı olarak sistemimizden kayıtlarını sildirebilirler.

                    Alınan finansal bilgiler satın alınan ürün ve hizmetlerin bedelinin tahsil edilmesinde ve diğer gerek duyulan durumlarda kullanılacaktır.

                    Kişiye özel bilgiler kullanıcılarımızın sisteme girişlerinde ve diğer gerektiği durumlarda kişinin kimliğinin doğrulanmasında kullanılacaktır. İstatistiki bilgiler ve profil bilgileri ayrıca sitemizin içinde de toplanmaktadır. Bu bilgileri istenilen tüm durumlarda kullanılabilir. Bu bilgiler ziyaretçi hareketlerinin izlenmesi, kişiye özel içerik sağlanması durumlarında kullanılacak, ayrıca tarafımızca belirlenen diğer firmalarla da paylaşılacaktır.
                    Bu paylaşım reklam veren kuruluşla yapılan anlaşmalar çerçevesinde gerçekleşecektir.</p>


            </div>
        </div>
    </div>
</article>

<hr>

<!-- Footer -->
@include('partials.footer')
<!-- jQuery -->
<script src="/cleanblog/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/cleanblog/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/cleanblog/js/jqBootstrapValidation.js"></script>
<script src="/cleanblog/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="/cleanblog/js/clean-blog.min.js"></script>

<!-- Extra plugin css -->
<script src="vendors/counterup/jquery.waypoints.min.js"></script>
<script src="vendors/counterup/jquery.counterup.min.js"></script>
<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
<script src="vendors/image-dropdown/jquery.dd.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
<script src="vendors/isotope/isotope.pkgd.min.js"></script>
<script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
<script src="vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
<script src="vendors/jquery-ui/jquery-ui.js"></script>
<script src="js/theme.js"></script>


</body>

</html>

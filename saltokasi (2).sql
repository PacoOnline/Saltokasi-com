-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 08 Eki 2020, 02:52:59
-- Sunucu sürümü: 10.4.14-MariaDB
-- PHP Sürümü: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `saltokasi`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(5, NULL, 1, 'ŞAL,EŞARP,FULAR AKSESUARLARI', 'aksesuar', '2020-09-16 06:35:18', '2020-10-02 04:58:53'),
(6, 6, 2, 'SAÇ TOKALARI', 'by-mi-naz-kundura', '2020-10-02 07:12:11', '2020-10-02 08:53:48'),
(7, NULL, 3, 'TAÇ ÇOCUK SAÇ TOKALARI', 'tac-cocuk-tokalari', '2020-10-02 11:05:33', '2020-10-02 11:09:40');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'short_description', 'text', 'Başlık', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'long_description', 'text_area', 'Açıklama', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'cover_photo', 'image', 'Kapak Fotoğrafı', 1, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'product_category', 'text', 'Product Category', 1, 1, 1, 1, 1, 1, '{}', 5),
(61, 7, 'slug', 'text', 'url', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"short_description\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:products,slug\"}}', 6),
(62, 7, 'images', 'multiple_images', 'Resimler', 1, 1, 1, 1, 1, 1, '{}', 7),
(63, 7, 'price', 'number', 'Fiyat', 1, 1, 1, 1, 1, 1, '{}', 8),
(64, 7, 'stock_number', 'number', 'Stok sayısı', 1, 1, 1, 1, 1, 1, '{}', 9),
(65, 7, 'product_belongsto_category_relationship', 'relationship', 'Ürünün kategorisi', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"product_category\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(66, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(67, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(68, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(69, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 2),
(70, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3),
(72, 8, 'title', 'text', 'Uzun Kenar- - Başlık', 1, 1, 1, 1, 1, 1, '{}', 5),
(73, 8, 'description', 'text', 'Uzun Kenar - yönlendirilecek ürünün url\'si', 0, 1, 1, 1, 1, 1, '{}', 6),
(74, 8, 'image_left', 'image', 'Sol Resim', 0, 1, 1, 1, 1, 1, '{}', 7),
(75, 8, 'title_left', 'text', 'Sol Resim - Başlık', 0, 1, 1, 1, 1, 1, '{}', 8),
(76, 8, 'image_right', 'image', 'Sağ Resim', 0, 1, 1, 1, 1, 1, '{}', 9),
(77, 8, 'title_right', 'text', 'Sağ Resim - Başlık', 0, 1, 1, 1, 1, 1, '{}', 10),
(78, 8, 'images', 'image', 'Uzun Kenar-  Resim', 1, 1, 1, 1, 1, 1, '{}', 3),
(79, 7, 'product_code', 'text', 'ürün kodu', 0, 1, 1, 1, 1, 1, '{}', 10),
(80, 14, 'id', 'hidden', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(81, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(82, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(83, 14, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 5),
(84, 14, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 6),
(85, 14, 'surname', 'text', 'Surname', 1, 1, 1, 1, 1, 1, '{}', 7),
(86, 14, 'city', 'text', 'City', 1, 1, 1, 1, 1, 1, '{}', 8),
(87, 14, 'address', 'text', 'Address', 1, 1, 1, 1, 1, 1, '{}', 9),
(88, 14, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 10),
(89, 14, 'phonenumber', 'text', 'Phonenumber', 1, 1, 1, 1, 1, 1, '{}', 11),
(90, 14, 'identitynumber', 'text', 'Identitynumber', 1, 1, 1, 1, 1, 1, '{}', 12),
(91, 14, 'notes', 'text', 'Notes', 1, 1, 1, 1, 1, 1, '{}', 13),
(92, 14, 'sepet_id', 'text', 'Sepet Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(93, 14, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 14),
(94, 14, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 15),
(95, 16, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(96, 16, 'image', 'image', 'Resim', 1, 1, 1, 1, 1, 1, '{}', 2),
(97, 16, 'titleofimage', 'text', 'Resimin başlığı(opsiyonel)', 0, 1, 1, 1, 1, 1, '{}', 3),
(98, 16, 'product_slug', 'text', 'Yönlendirmek istediğinizi ürün(opsiyonel)', 0, 1, 1, 1, 1, 1, '{}', 4),
(99, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(100, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(7, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-16 06:26:51', '2020-09-30 10:13:39'),
(8, 'featured_images', 'featured-images', 'Featured Image', 'Featured Images', 'voyager-book', 'App\\FeaturedImage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-17 14:33:34', '2020-10-07 21:48:50'),
(14, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(15, 'slider', 'slider', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(16, 'sliders', 'sliders', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-07 21:38:28', '2020-10-07 21:39:49');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `featured_images`
--

CREATE TABLE `featured_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_left` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_left` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_right` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_right` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `featured_images`
--

INSERT INTO `featured_images` (`id`, `created_at`, `images`, `title`, `description`, `image_left`, `title_left`, `image_right`, `title_right`, `updated_at`) VALUES
(3, '2020-09-17 18:31:00', 'featured-images\\October2020\\wbOnn1QohxfwfOOmRFum.jpg', 'ÇOK SATANLAR', 'BURAYA TIKLAYARAK EN ÇOK SATANLARA ULAŞABİLİRSİNİZ', 'featured-images\\October2020\\hO4KByErAw1Vvitt3ud5.jpg', 'AYIN ÜRÜNÜ', 'featured-images\\September2020\\WCoMEMFBLyYP2nBKg5be.jpeg', 'İNDİRİMLİ ÜRÜNLER', '2020-10-07 21:49:07');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `frontend_users`
--

CREATE TABLE `frontend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activision_key` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` int(11) DEFAULT 0,
  `phone_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-09-16 06:08:36', '2020-09-16 06:08:36');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Anasayfa', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2020-09-16 06:08:36', '2020-09-16 06:40:47', 'voyager.dashboard', 'null'),
(2, 1, 'Medya', '', '_self', 'voyager-images', '#000000', NULL, 4, '2020-09-16 06:08:36', '2020-09-16 06:41:27', 'voyager.media.index', 'null'),
(3, 1, 'Kullanıcılar', '', '_self', 'voyager-person', '#000000', NULL, 3, '2020-09-16 06:08:36', '2020-09-16 06:41:18', 'voyager.users.index', 'null'),
(4, 1, 'Roller', '', '_self', 'voyager-lock', '#000000', NULL, 2, '2020-09-16 06:08:36', '2020-09-16 06:41:11', 'voyager.roles.index', 'null'),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2020-09-16 06:08:36', '2020-09-17 15:31:24', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-09-16 06:08:36', '2020-09-16 06:30:25', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-09-16 06:08:36', '2020-09-16 06:30:25', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-09-16 06:08:36', '2020-09-16 06:30:25', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-09-16 06:08:36', '2020-09-16 06:30:25', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2020-09-16 06:08:36', '2020-09-17 15:31:24', 'voyager.settings.index', NULL),
(11, 1, 'Kategoriler', '', '_self', 'voyager-categories', '#000000', NULL, 9, '2020-09-16 06:08:36', '2020-09-29 11:04:44', 'voyager.categories.index', 'null'),
(12, 1, 'Yazılar', '', '_self', 'voyager-news', '#000000', NULL, 5, '2020-09-16 06:08:36', '2020-09-16 06:42:51', 'voyager.posts.index', 'null'),
(13, 1, 'Sayfalar', '', '_self', 'voyager-file-text', '#000000', NULL, 8, '2020-09-16 06:08:37', '2020-09-17 15:31:24', 'voyager.pages.index', 'null'),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-09-16 06:08:37', '2020-09-16 06:30:25', 'voyager.hooks', NULL),
(15, 1, 'Ürünler', '', '_self', 'voyager-bag', '#000000', NULL, 6, '2020-09-16 06:26:51', '2020-09-16 06:43:00', 'voyager.products.index', 'null'),
(16, 1, 'Öne Çıkan Resimler', '', '_self', 'voyager-book', '#000000', NULL, 7, '2020-09-17 14:33:34', '2020-09-17 15:31:24', 'voyager.featured-images.index', 'null'),
(17, 1, 'Orders', '', '_self', NULL, NULL, NULL, 12, '2020-10-07 21:32:56', '2020-10-07 21:32:56', 'voyager.orders.index', NULL),
(19, 1, 'Sliders', '', '_self', NULL, NULL, NULL, 14, '2020-10-07 21:38:28', '2020-10-07 21:38:28', 'voyager.sliders.index', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_08_19_000000_create_failed_jobs_table', 1),
(28, '2020_10_02_124426_create_shopping_card', 2),
(29, '2020_10_02_124626_create_shopping_card_product', 2),
(30, '2020_10_06_182516_create_orders_table', 2),
(31, '2020_10_06_232551_create_slider_table', 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identitynumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sepet_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(5,4) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-09-16 06:08:37', '2020-09-16 06:08:37');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(2, 'browse_bread', NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(3, 'browse_database', NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(4, 'browse_media', NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(5, 'browse_compass', NULL, '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(6, 'browse_menus', 'menus', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(7, 'read_menus', 'menus', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(8, 'edit_menus', 'menus', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(9, 'add_menus', 'menus', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(10, 'delete_menus', 'menus', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(11, 'browse_roles', 'roles', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(12, 'read_roles', 'roles', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(13, 'edit_roles', 'roles', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(14, 'add_roles', 'roles', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(15, 'delete_roles', 'roles', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(16, 'browse_users', 'users', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(17, 'read_users', 'users', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(18, 'edit_users', 'users', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(19, 'add_users', 'users', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(20, 'delete_users', 'users', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(21, 'browse_settings', 'settings', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(22, 'read_settings', 'settings', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(23, 'edit_settings', 'settings', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(24, 'add_settings', 'settings', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(25, 'delete_settings', 'settings', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(26, 'browse_categories', 'categories', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(27, 'read_categories', 'categories', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(28, 'edit_categories', 'categories', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(29, 'add_categories', 'categories', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(30, 'delete_categories', 'categories', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(31, 'browse_posts', 'posts', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(32, 'read_posts', 'posts', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(33, 'edit_posts', 'posts', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(34, 'add_posts', 'posts', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(35, 'delete_posts', 'posts', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(36, 'browse_pages', 'pages', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(37, 'read_pages', 'pages', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(38, 'edit_pages', 'pages', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(39, 'add_pages', 'pages', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(40, 'delete_pages', 'pages', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(41, 'browse_hooks', NULL, '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(42, 'browse_products', 'products', '2020-09-16 06:26:51', '2020-09-16 06:26:51'),
(43, 'read_products', 'products', '2020-09-16 06:26:51', '2020-09-16 06:26:51'),
(44, 'edit_products', 'products', '2020-09-16 06:26:51', '2020-09-16 06:26:51'),
(45, 'add_products', 'products', '2020-09-16 06:26:51', '2020-09-16 06:26:51'),
(46, 'delete_products', 'products', '2020-09-16 06:26:51', '2020-09-16 06:26:51'),
(47, 'browse_featured_images', 'featured_images', '2020-09-17 14:33:34', '2020-09-17 14:33:34'),
(48, 'read_featured_images', 'featured_images', '2020-09-17 14:33:34', '2020-09-17 14:33:34'),
(49, 'edit_featured_images', 'featured_images', '2020-09-17 14:33:34', '2020-09-17 14:33:34'),
(50, 'add_featured_images', 'featured_images', '2020-09-17 14:33:34', '2020-09-17 14:33:34'),
(51, 'delete_featured_images', 'featured_images', '2020-09-17 14:33:34', '2020-09-17 14:33:34'),
(52, 'browse_orders', 'orders', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(53, 'read_orders', 'orders', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(54, 'edit_orders', 'orders', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(55, 'add_orders', 'orders', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(56, 'delete_orders', 'orders', '2020-10-07 21:32:56', '2020-10-07 21:32:56'),
(57, 'browse_slider', 'slider', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(58, 'read_slider', 'slider', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(59, 'edit_slider', 'slider', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(60, 'add_slider', 'slider', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(61, 'delete_slider', 'slider', '2020-10-07 21:33:31', '2020-10-07 21:33:31'),
(62, 'browse_sliders', 'sliders', '2020-10-07 21:38:28', '2020-10-07 21:38:28'),
(63, 'read_sliders', 'sliders', '2020-10-07 21:38:28', '2020-10-07 21:38:28'),
(64, 'edit_sliders', 'sliders', '2020-10-07 21:38:28', '2020-10-07 21:38:28'),
(65, 'add_sliders', 'sliders', '2020-10-07 21:38:28', '2020-10-07 21:38:28'),
(66, 'delete_sliders', 'sliders', '2020-10-07 21:38:28', '2020-10-07 21:38:28');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(42, 3),
(43, 1),
(43, 3),
(44, 1),
(44, 3),
(45, 1),
(45, 3),
(46, 1),
(46, 3),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-09-16 06:08:37', '2020-09-16 06:08:37');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `short_description` varchar(100) NOT NULL,
  `long_description` varchar(10001) NOT NULL,
  `cover_photo` varchar(300) NOT NULL,
  `product_category` int(15) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `images` varchar(5000) NOT NULL,
  `price` int(11) NOT NULL,
  `stock_number` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_code` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `products`
--

INSERT INTO `products` (`id`, `short_description`, `long_description`, `cover_photo`, `product_category`, `slug`, `images`, `price`, `stock_number`, `created_at`, `updated_at`, `product_code`) VALUES
(14, 'YEŞİL ASRİN MODEL', 'FAZİ, YEŞİL ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/aojum7J06jNBE6Zi4qHA.jpg', 5, 'yesi-l-asri-n-model', '[\"products\\/October2020\\/MwTdtsufQq8bUZAwAFby.jpg\"]', 70, 6, '2020-10-01 04:22:00', '2020-10-01 05:00:18', 'K010170'),
(15, 'ÇUBUKLU ASRİN', 'FAZİ, ÇUBUKLU ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/UC3p4GKgvDVPqd6shuWe.png', 5, 'cubuklu-asri-n', '[\"products\\/October2020\\/clVObXzcNwcNVrVqznea.jpg\"]', 70, 2, '2020-10-01 04:57:00', '2020-10-01 05:00:38', 'KT010170'),
(16, 'SİYAH ASRİN', 'FAZİ, SİYAH ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/JfDmeW5xghEYcFkaT6bL.jpg', 5, 'si-yah-asri-n', '[\"products\\/October2020\\/rMiyFKvfYUveAToG1m7z.jpeg\"]', 70, 2, '2020-10-01 04:59:56', '2020-10-01 04:59:56', 'K010170S'),
(17, 'GÜMÜŞ ASRİN', 'FAZİ, GÜMÜŞ ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/Vm5SDHm2b46O3sJJ1vo9.jpg', 5, 'gumus-asri-n', '[\"products\\/October2020\\/6Soo2kZcxZ1rkvKzwjkz.jpg\"]', 70, 3, '2020-10-01 05:02:00', '2020-10-02 04:37:51', 'K010170K'),
(18, 'KIRMIZI ASRİN', 'FAZİ, KIRMIZI ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/UkfbCW3Hfo5c8SMfcIw8.jpg', 5, 'kirmizi-asri-n', '[\"products\\/October2020\\/vABcmE4As6JM7o7RKtpa.jpg\"]', 70, 5, '2020-10-02 04:39:00', '2020-10-02 04:40:32', 'K010170L'),
(19, 'MAVİ ASRİN', 'FAZİ, MAVİ ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/37Mtc525NR3zlv7jRo1f.jpg', 5, 'mavi-asri-n', '[\"products\\/October2020\\/vtqDywLga76eP2AYXnNZ.jpg\"]', 70, 1, '2020-10-02 04:42:04', '2020-10-02 04:42:04', 'K010170M'),
(20, 'FUŞYA ASRİN', 'FAZİ, FUŞYA ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/6stQlSx9sAgUccrpc2dS.jpg', 5, 'fusya-asri-n', '[\"products\\/October2020\\/fJ5vRta0KmxUhV02KJPn.jpg\"]', 70, 8, '2020-10-02 04:45:16', '2020-10-02 04:45:16', 'K010170N'),
(21, 'MAVİ ASRİN 2', 'FAZİ, MAVİ ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/5qk6UwUMhZR4R6bWBSup.jpg', 5, 'mavi-asri-n-2', '[\"products\\/October2020\\/HCQzLAaG8Wle1KyMokV2.jpg\"]', 70, 1, '2020-10-02 04:51:07', '2020-10-02 04:51:07', 'K010170Z'),
(22, 'ASRİN KARMA', 'FAZİ, KARMA ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/RwRXBP2Wbt6b7VWHJrDZ.jpg', 5, 'asri-n-karma', '[\"products\\/October2020\\/tavIu9KFSg3eJKB1dzTc.jpg\"]', 70, -2, '2020-10-02 04:57:40', '2020-10-02 04:57:40', 'K010170P'),
(23, 'KIZIL ASRİN', 'FAZİ, KIZIL ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/Xw9MtPruBFWLXaFMbKOu.jpg', 5, 'kizil-asri-n', '[\"products\\/October2020\\/RfnuOAy1uCuy9GxXYCGr.jpg\"]', 70, 0, '2020-10-02 05:02:15', '2020-10-02 05:02:15', 'K010170B'),
(24, 'KAHVE ASRİN', 'FAZİ, KAHVE ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/qjJxcEZC4iv003T0TNLX.jpg', 5, 'kahve-asri-n', '[\"products\\/October2020\\/eIgdZVEQNzGcWqv148D8.jpg\"]', 70, 1, '2020-10-02 05:04:43', '2020-10-02 05:04:43', 'K010170C'),
(25, 'UZUN ÇUBUK ASRİN', 'FAZİ, UZUN ÇUBUK ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/UEYbQwC5y3IjRaEg6Jkz.jpg', 5, 'uzun-cubuk-asri-n', '[\"products\\/October2020\\/iOlxiIHyWIaFs14K4J9K.jpg\"]', 70, 1, '2020-10-02 05:06:44', '2020-10-02 05:06:44', 'K010170D'),
(26, 'YEŞİL KAHVE ASRİN', 'FAZİ, YEŞİL KAHVE ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/ZwO87rs3KdjCQEb9OCvd.jpg', 5, 'yesi-l-kahve-asri-n', '[\"products\\/October2020\\/La3HtEndfAS5B6KjGEHW.jpg\"]', 70, 1, '2020-10-02 05:10:22', '2020-10-02 05:10:22', 'K010170A'),
(27, 'KEMERLİ ASRİN', 'FAZİ, SARI KEMERLİ ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/xBkTFzvHeqxPq1PyOShO.jpg', 5, 'kemerli-asri-n', '[\"products\\/October2020\\/WGFJ170Db1YoKl3xAN9U.jpg\"]', 70, 1, '2020-10-02 05:13:15', '2020-10-02 05:13:15', 'K010170E'),
(28, 'ASRİN V', 'FAZİ,  ASRİN V MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/MUHaI93GF0ql82SMeGo4.jpg', 5, 'asri-n-v', '[\"products\\/October2020\\/6ZRfixdlXoBPgJjL5F0N.jpg\"]', 70, 1, '2020-10-02 05:16:59', '2020-10-02 05:16:59', 'K010170F'),
(29, 'PEMBE SİYAH ASRİN', 'FAZİ, PEMBE SİYAH ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/xZAqA6webentgP8M1kY8.jpg', 5, 'pembe-si-yah-asri-n', '[\"products\\/October2020\\/GjWCux7XcolMo29a3lzF.jpg\"]', 70, 4, '2020-10-02 05:19:21', '2020-10-02 05:19:21', 'K010170G'),
(30, 'KRİSTAL ASRİN', 'FAZİ, KRİSTAL ASRİN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/q6HptG6tlk5KGMJX9cb5.jpeg', 5, 'kri-stal-asri-n', '[\"products\\/October2020\\/sAkasHM7pLFhUTVH2s96.jpg\"]', 70, 12, '2020-10-02 05:24:09', '2020-10-02 05:24:09', 'K010170H'),
(31, 'ASRİN BİR', 'FAZİ,  ASRİN BİR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/xWWQstqpTkVF9WehtJ02.jpg', 5, 'asri-n-bi-r', '[\"products\\/October2020\\/PEW3GrUB3JGRcuBy3Ebu.jpg\"]', 70, 12, '2020-10-02 05:29:32', '2020-10-02 05:29:32', 'K010170J'),
(32, 'AÇELYA', 'FAZİ, AÇELYA MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/H09Rtw3fbze91wzFmPv7.png', 5, 'AÇELYA', '[\"products\\/October2020\\/J4wnjcORsm4jleRkBivO.jpg\"]', 35, 2, '2020-10-02 05:36:15', '2020-10-02 05:36:15', 'K033035'),
(33, 'BEYAZ ZÜLAL', 'FAZİ, BEYAZ ZÜLAL MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/xHAnQ670ah8bh0YVBTRM.png', 5, 'beyaz-zulal', '[\"products\\/October2020\\/IeS2F0UogCqQS5UxwyXW.png\"]', 70, 4, '2020-10-02 05:47:44', '2020-10-02 05:47:44', 'K034570'),
(34, 'SİYAH ÇINAR', 'FAZİ, SİYAH ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/51zEQi30iFHGWVSxBnSm.jpg', 5, 'si-yah-cinar', '[\"products\\/October2020\\/inHEej9KzQDGj2eIDQz3.jpg\"]', 35, 3, '2020-10-02 05:51:09', '2020-10-02 05:51:09', 'K020235A'),
(35, 'GÜMÜŞ ÇINAR', 'FAZİ, GÜMÜŞ ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/UeXRnoNBPEMUAEVuJPCa.jpg', 5, 'gumus-cinar', '[\"products\\/October2020\\/WIr8CWD2kqdDELhDkQ5Q.jpg\"]', 35, 1, '2020-10-02 05:56:55', '2020-10-02 05:56:55', 'K020235B'),
(36, 'ANTİK ÇINAR', 'FAZİ, ANTİK ÇINAR YAPRAĞI MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/jFRDVvH6gir8a5dPlfYh.jpeg', 5, 'anti-k-cinar', '[\"products\\/October2020\\/Ds01XD5kh9vnam1DotR3.jpg\"]', 35, 5, '2020-10-02 06:02:29', '2020-10-02 06:02:29', 'K020235C'),
(37, 'KAHVE ÇINAR', 'FAZİ, KAHVE ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/Pr2tbh59kkAR37uqx4Wd.jpg', 5, 'kahve-cinar', '[\"products\\/October2020\\/ulY4QP1YvGwbdHAUSZ9X.jpg\"]', 35, 3, '2020-10-02 06:08:20', '2020-10-02 06:08:20', 'K020235D'),
(38, 'GRİ ÇINAR', 'FAZİ, GRİ ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/xjjKMye8F1WFhXSYH4wt.jpg', 5, 'gri-cinar', '[\"products\\/October2020\\/El4mbGedDWX3I5mqsZP0.jpg\"]', 35, 9, '2020-10-02 06:09:49', '2020-10-02 06:09:49', 'K020235E'),
(39, 'BAKIR ÇINAR', 'FAZİ, BAKIR ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/3mDJNb6AN8enm3FEo8t2.jpg', 5, 'bakir-cinar', '[\"products\\/October2020\\/6gIGwfepj5NaSDGiCwIU.jpg\"]', 35, 5, '2020-10-02 06:11:39', '2020-10-02 06:11:39', 'K020235F'),
(40, 'ALTIN ÇINAR', 'FAZİ, ALTIN ÇINAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/qFKmKPGIduJDZIEW3ie0.jpg', 5, 'altin-cinar', '[\"products\\/October2020\\/OdwNCiO1L6KnvXgbIbDq.jpg\"]', 35, 2, '2020-10-02 06:21:05', '2020-10-02 06:21:05', 'K020235G'),
(41, 'DENİZ MODEL', 'FAZİ, deniz model MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/yRttdv4JStXZuLpjTP36.png', 5, 'deni-z-model', '[\"products\\/October2020\\/Im7Fvg2Hj6c44QO02el4.png\",\"products\\/October2020\\/W9BPLGpaKK9aUF9kHHAd.jpg\"]', 35, 5, '2020-10-02 06:23:00', '2020-10-02 06:24:07', 'k021535'),
(42, 'ELA ŞERİT MODEL', 'FAZİ, ELA ŞERİT MODEL MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/eSf1aXMEEqgmFsNB5Dm6.jpeg', 5, 'ela-seri-t-model', '[\"products\\/October2020\\/6YzSKshvpAo3DMEU6Hvx.jpeg\",\"products\\/October2020\\/4xfUMuvhqRW2WBPcuQ1U.jpeg\",\"products\\/October2020\\/1PyL3CIJsCkt9EUrk1QJ.jpg\"]', 35, 2, '2020-10-02 06:26:00', '2020-10-05 10:24:22', 'K020735'),
(43, 'FERAYE MODEL', 'FAZİ,FERAYE MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/5XPT7LahibAEIvbS6f2u.png', 5, 'feraye-model', '[\"products\\/October2020\\/2fUINTuREotInGJdMvIN.jpg\",\"products\\/October2020\\/ImNnGzgHJez9Ibpl66Ls.png\"]', 50, 4, '2020-10-02 06:29:00', '2020-10-02 06:30:29', 'K032550'),
(44, 'FUŞYA BAHAR', 'FAZİ, FUŞYA BAHAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/YB65cUTacLYs0p9gIn9r.png', 5, 'fusya-bahar', '[\"products\\/October2020\\/SVVVYbO8JHyOLIDTG5v2.jpg\"]', 50, 1, '2020-10-02 06:35:03', '2020-10-02 06:35:03', 'K021150'),
(45, 'GRİ İNCİLİ ŞERİT', 'FAZİ, GRİ İNCİLİ ŞERİT MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/67QcVbxWY6N4wySsZ1Tp.png', 5, 'gri-i-nci-li-seri-t', '[\"products\\/October2020\\/osCs3fQxgjFHVdRY1ZPW.jpg\"]', 25, 2, '2020-10-02 06:46:57', '2020-10-02 06:46:57', 'K032225'),
(46, 'GÜZ MODEL', 'FAZİ, GÜZ MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/rygrg2wCYpdfwa0ut5um.png', 5, 'guz-model', '[\"products\\/October2020\\/uYTOx6kFtZ4h8MAxTJbg.png\"]', 35, 3, '2020-10-02 07:00:44', '2020-10-02 07:00:44', 'K021435'),
(47, 'GÜZİ MODEL', 'FAZİ, GÜZİ MODEL MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/ENb3ho68Jchfm7AooBLP.jpg', 5, 'guzi-model', '[\"products\\/October2020\\/hKlVShvLDdnPp6oxeHdC.jpg\",\"products\\/October2020\\/doHEN5VUrcrpKTyCxAJB.jpg\"]', 60, 1, '2020-10-02 07:07:00', '2020-10-02 07:08:30', 'K010360'),
(48, 'İNCİLİ SULTAN', 'FAZİ, İNCİLİ SULTAN MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/WFfTSCIOBfzDzZtnTexa.jpg', 5, 'i-nci-li-sultan', '[\"products\\/October2020\\/g5cSY5OOJHD11I8sgQKv.jpeg\"]', 35, 1, '2020-10-02 07:11:10', '2020-10-02 07:11:10', 'K020635'),
(49, 'İNCİLİ ŞERİT', 'FAZİ,  İNCİLİ ŞERİT MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/lxYXhrV3II9yWA172McQ.png', 5, 'i-nci-li-seri-t', '[\"products\\/October2020\\/QC2mp3kzexdJEasyr24f.png\"]', 30, 4, '2020-10-02 07:16:41', '2020-10-02 07:16:41', 'K032030'),
(50, 'KARA SEVDA', 'FAZİ, KARA SEVDA MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/aSUPtW1eyfebKQTF3PYl.jpg', 5, 'kara-sevda', '[\"products\\/October2020\\/rqejD1XMDYVXaFaYuDzz.jpg\"]', 30, 3, '2020-10-02 07:18:48', '2020-10-02 07:18:48', 'K031730'),
(51, 'KIRMIZI İNCİLİ ŞERİT', 'FAZİ,KIRMIZI İNCİLİ ŞERİT MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/1XnbQceQsQ99VDUxe5Zu.png', 5, 'kirmizi-i-nci-li-seri-t', '[\"products\\/October2020\\/9z8yVcsFxNtnDhp7YkxW.png\"]', 35, 2, '2020-10-02 07:20:31', '2020-10-02 07:20:31', 'K034135'),
(52, 'KRİSTAL MODEL', 'FAZİ, KRİSTAL MODEL  kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/MrCGXJK27Mh97MN6zqX4.jpeg', 5, 'kri-stal-model', '[\"products\\/October2020\\/7T8orMkCBYMl1D02p89j.JPG\",\"products\\/October2020\\/2z2wnFDdA1hGjxVV0LU7.jpeg\",\"products\\/October2020\\/phHi4nfnvg94ysLpDV6x.jpeg\"]', 60, 4, '2020-10-02 07:23:00', '2020-10-02 07:44:45', 'K010460');
INSERT INTO `products` (`id`, `short_description`, `long_description`, `cover_photo`, `product_category`, `slug`, `images`, `price`, `stock_number`, `created_at`, `updated_at`, `product_code`) VALUES
(53, 'KRİZANTEM MODEL', 'FAZİ, KRİZANTEM MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/N8pt4h6p9MM37CytSSEL.jpeg', 5, 'kri-zantem-model', '[\"products\\/October2020\\/nFq5XS3zFBqutH53A4CH.jpeg\",\"products\\/October2020\\/NtUE9GxM6iRuJf1IOJJo.jpeg\",\"products\\/October2020\\/wJUPusTbilbqDFNQNLAz.jpeg\"]', 20, 3, '2020-10-02 07:44:00', '2020-10-02 08:25:47', 'K020520'),
(54, 'MANOLYA', 'FAZİ, MANOLYA kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/ZUnYexfU50BqJNSIH0Kh.png', 5, 'manolya', '[\"products\\/October2020\\/e9e4UlRon3MjsQxgqVsn.png\"]', 40, 1, '2020-10-02 07:55:37', '2020-10-02 07:55:37', 'K033640'),
(55, 'NAİF MODEL', 'FAZİ, NAİF MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/6a3O00mMNQ7sKihFt6ao.png', 5, 'nai-f-model', '[\"products\\/October2020\\/5b0zzMzgdhAwgoKiZXXC.png\"]', 40, 1, '2020-10-02 07:59:59', '2020-10-02 07:59:59', 'K033740'),
(56, 'SARI NEGİZ MODEL', 'FAZİ, SARI NERGİZ kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/7XWvI1PygyfJw1FTabxT.png', 5, 'sari-negi-z-model', '[\"products\\/October2020\\/ic6GP1MCQ8xD8KKn0guT.png\"]', 20, 1, '2020-10-02 08:04:38', '2020-10-02 08:04:38', 'K032120'),
(57, 'MAVİ NEVA AÇIK', 'FAZİ, MAVİ NEVA AÇIK kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/ER38n1Jebe6bBXDjLLWJ.jpg', 5, 'mavi-neva-acik', '[\"products\\/October2020\\/tw3ElQ0BpfvIfPuMYvmN.jpg\"]', 35, 1, '2020-10-02 08:07:56', '2020-10-02 08:07:56', 'K010535'),
(58, 'BEYAZ NEVA', 'FAZİ, BEYAZ NEVA kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/rmVkfQXeJH2ND48bQt7d.jpg', 5, 'beyaz-neva', '[\"products\\/October2020\\/LX17m2dzFIEGFAoqA69h.jpg\"]', 35, 3, '2020-10-02 08:11:22', '2020-10-02 08:11:22', 'K010535A'),
(59, 'PEMBE NEVA AÇIK', 'FAZİ, PEMBE NEVA AÇIK kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/A498SMHKF7YZ5pXAWPCe.jpg', 5, 'pembe-neva-acik', '[\"products\\/October2020\\/XYmZvqvGbpOGwu8KFCzy.jpeg\"]', 35, 1, '2020-10-02 08:14:15', '2020-10-02 08:14:15', 'K010535B'),
(60, 'GRİ NEVA', 'FAZİ, GRİ NEVA kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/vLIuHV5mISyW7Q6cOmFs.jpg', 5, 'gri-neva', '[\"products\\/October2020\\/06qPEYkDaDRgHuDBvwmH.jpg\"]', 35, 16, '2020-10-02 08:19:07', '2020-10-02 08:19:07', 'K010535C'),
(61, 'PEMBE NEVA KAPALI', 'FAZİ, PEMBE NEVA KAPALI kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/EXiDw55137H1ULxf1VkK.jpeg', 5, 'pembe-neva-kapali', '[\"products\\/October2020\\/unTYemet6L7qtwWBgP7q.jpeg\"]', 35, 9, '2020-10-02 08:20:54', '2020-10-02 08:20:54', 'K010535E'),
(62, 'YEŞİL NEVA', 'FAZİ,YEŞİL NEVA kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/tcd17wlEHTJGoE3oT69z.jpg', 5, 'yesi-l-neva', '[\"products\\/October2020\\/W0r9AeoRGFctO4QiyoQz.jpeg\"]', 35, 5, '2020-10-02 08:23:13', '2020-10-02 08:23:13', 'K010535D'),
(63, 'YEŞİL KRİZANTEM', 'FAZİ,YEŞİL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/IX7WiWvIWsZSqJla9Oem.jpg', 5, 'yesi-l-kri-zantem', '[\"products\\/October2020\\/Oj5sVOqdFf2FN86Ezj7w.jpeg\"]', 20, 6, '2020-10-02 08:28:22', '2020-10-02 08:28:22', 'K020520Y'),
(64, 'OKYANUS MODEL', 'FAZİ, OKYANUS MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/dubDZjmWUTrg9D2HqqtC.jpg', 5, 'okyanus-model', '[\"products\\/October2020\\/GOe2XbNPBIoGkdAhCXsm.jpg\"]', 35, 4, '2020-10-02 08:30:00', '2020-10-02 08:32:26', 'K020935'),
(65, 'RENGAHENK', 'FAZİ, RENGAHENK MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/q1PEQX3dXUHroAu1pgDl.jpg', 5, 'rengahenk', '[\"products\\/October2020\\/e9o8iiQDqmeqStBriWbB.jpeg\"]', 35, 5, '2020-10-02 08:40:56', '2020-10-02 08:40:56', 'K020435'),
(66, 'SAADET MODEL', 'FAZİ, SAADET MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/ISYZ7MaTJMj3dckSqNv9.png', 5, 'saadet-model', '[\"products\\/October2020\\/BcN1USrghuNLwxIlLaeG.png\"]', 25, 1, '2020-10-02 08:50:33', '2020-10-02 08:50:33', 'K031925'),
(67, 'SİYAH İNCİ', 'FAZİ, SİYAH İNCİ MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/sRt2WvxwVeTap34ROfy1.png', 5, 'si-yah-i-nci', '[\"products\\/October2020\\/2jW1y78mYjfJZpO2R0wL.png\"]', 35, 2, '2020-10-02 08:52:56', '2020-10-02 08:52:56', 'K033935'),
(68, 'PEMBE TOPUZ SAÇ TOKASI', 'Fazi topuz saç tokaları\r\nkullanımı çok kolay', 'products/October2020/Ci4mLpCDGO7M5WBh13SP.jpg', 6, 'pembe-topuz-sac-tokasi', '[\"products\\/October2020\\/GWmPZ6SmeTmclNEpqX49.jpg\",\"products\\/October2020\\/h4heKlg7oQEXg9rUpcTQ.jpg\"]', 35, 10, '2020-10-02 08:58:00', '2020-10-02 08:59:34', 'T010335'),
(69, 'İNCİLİ TOPUZ', 'TOPUZ SAÇLAR İÇİN \r\nGÜZEL BİR TASARIM\r\nİNCİLİ TOPUZ SAÇ TOKASI\r\nFAZİ TASARIMDAN', 'products/October2020/pOPmZP60SwwNQerrRNfS.png', 6, 'i-nci-li-topuz', '[\"products\\/October2020\\/9jv2WmEcCiYoJhJsfEYt.png\"]', 35, 4, '2020-10-02 09:04:49', '2020-10-02 09:04:49', 'T010435'),
(70, 'SİYAH ŞERİT MODEL', 'FAZİ, SİYAH ŞERİT MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/cvhgBUgkklm4IDMDFhsb.png', 5, 'si-yah-seri-t-model', '[\"products\\/October2020\\/pAWtGMMxNM9OnILJCIHp.jpg\"]', 35, 1, '2020-10-02 10:16:58', '2020-10-02 10:16:58', 'K032335'),
(71, 'ŞULE MODEL', 'FAZİ, ŞULE MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/5Dlp4jcD1wXSrMcHoy0g.jpg', 5, 'sule-model', '[\"products\\/October2020\\/RgWNjP2VaxzKy5FDLRGT.jpg\"]', 35, -1, '2020-10-02 10:29:07', '2020-10-02 10:29:07', 'K032435'),
(72, 'TAŞLI TAÇ MODEL', 'FAZİ, TAŞLI TAÇ MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/dkKTmu51dWgrXYyxEDxN.jpg', 5, 'tasli-tac-model', '[\"products\\/October2020\\/LALSECPPHKoRkXzExc0C.png\"]', 60, 3, '2020-10-02 10:33:11', '2020-10-02 10:33:11', 'K032860'),
(73, 'TOPRAK ŞERİT MODEL', 'FAZİ, TOPRAK ŞERİT MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/nhltyPSfmWQnTK8kBOtr.jpg', 5, 'toprak-seri-t-model', '[\"products\\/October2020\\/wY9jeGzwJTvVJnvKlY35.jpg\"]', 35, 1, '2020-10-02 10:38:00', '2020-10-05 07:15:20', 'K020135'),
(74, 'TURUNCU GÜZ MODEL', 'FAZİ, TURUNCU GÜZ MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/bVujDqJeWAv319A6oGjr.png', 5, 'turuncu-guz-model', '[\"products\\/October2020\\/FqBBdhYPeiFACzgsFAoy.jpg\"]', 35, 1, '2020-10-02 10:40:54', '2020-10-02 10:40:54', 'K032935'),
(75, 'TÜRKÜ MODEL', 'FAZİ, TÜRKÜ MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/DQCC1jH6Rcr3CPz6sUZO.jpg', 5, 'turku-model', '[\"products\\/October2020\\/hwfmLVGBzLvJacIPx7yl.jpg\"]', 30, 6, '2020-10-02 10:43:28', '2020-10-02 10:43:28', 'K020830'),
(76, 'YEŞİL BAHAR MODEL', 'FAZİ, YEŞİL BAHAR MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/b2fmZZY9J78z7XDp1ljz.png', 5, 'yesi-l-bahar-model', '[\"products\\/October2020\\/6I6uor3xh4AKqphO7LHu.jpg\"]', 50, 1, '2020-10-02 10:47:14', '2020-10-02 10:47:14', 'K033850'),
(77, 'ZÜLAL MODEL', 'FAZİ, ZÜLAL MODEL kadın, şal/eşarp/fular tokası					\r\nIğnesiz, pratik, stoper ayarlı, şal/eşarp/fular takısı					\r\nŞık ve Zarif hanımların tercihi,   Fazi Şal ve Eşarp tokaları,					\r\nİĞNESİZDİR , kumaşı zedelemez!					\r\nÜrünlerimiz; el işçiliği ile işlenmiştir.Şal, eşarp veya fular  taktığınızda şeklini uzun süre koruyan, \r\nkombinlerinizi şık ve zarif şekilde tamamlayan,   tasarım tescilli ve patentli    \r\nürünümüz stoper ayarı ile o kadar pratik bir kullanım sunuyor ki bayılacaksınız. Hem şık hem kurtarıcı bu ürünün kullanıcılarına kolaylık sağlaması hasebiyle mutluyuz...					\r\nGüzel günlerde kullanmanız dileğiyle…  					\r\nKullanımı: Şal veya eşarbınızın iki ucunu beraber geçirin, stoperi basılı tutarak istediğiniz sıkılığa getirin ve şekillendirin. \r\nDiğer  şekil için: önce bir ucunu geçirin ve diğer ucu da çapraz olarak stoper ayarlı halkadan geçirip  \r\nstoperi basılı tutarak istediğiniz sıkılığa getirin ve bırakın sonra şal veya eşarbınıza  şekil verin ve rahatlığa adım atın.', 'products/October2020/EyDY3HBAott5Mnf3m5I0.jpg', 5, 'zulal-model', '[\"products\\/October2020\\/pVZlBjHT5C3Q3NGHG83U.jpg\"]', 70, 6, '2020-10-02 10:50:18', '2020-10-02 10:50:18', 'K021270'),
(78, 'PUANTİYELİ TOPUZ ÇİÇEK', '90\'ların modası topuz tokaları geri geliyor. Birbirinden şık otomatik topuz tokalarını sizler için tasarladık.						\r\nÖzel günlerinizde de gündelik hayatınızda da rahatlıkla kullanabileceğiniz bu tokalar sizi çok havalı gösterecek.						\r\nŞık olmanın en kolay yolu...						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/7p6MrxDSSKfNA3aFMU2n.jpg', 6, 'puanti-yeli-topuz-ci-cek', '[\"products\\/October2020\\/x7qy6jVvdNstGIgFCHO0.jpg\"]', 30, 10, '2020-10-02 10:52:00', '2020-10-04 10:09:38', 'T010525'),
(79, 'PUANTİYELİ FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/Vx0GSkFRodOc8v9q7p0S.jpg', 6, 'puanti-yeli-fi-yonk-toka', '[\"products\\/October2020\\/LoMT4hCwEkN2sScvIxKw.jpg\"]', 15, 9, '2020-10-02 10:54:00', '2020-10-02 18:02:34', 'T010615'),
(80, 'BAHAR MODEL', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/vVbCckWMltei8sTEFs3D.png', 6, 'bahar-model', '[\"products\\/October2020\\/soaZHgRXqbJNCuMv8lDG.jpg\",\"products\\/October2020\\/cz3U091d5QSyzpV4UTEW.jpg\",\"products\\/October2020\\/MF2IcOUCHjAbVmXz9ThR.jpg\"]', 15, 4, '2020-10-02 10:57:00', '2020-10-02 18:02:52', 'T011115'),
(81, 'KIZIL FİYONK', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/LAYCXjQiuAWfNElaX5Jz.jpg', 6, 'kizil-fi-yonk', '[\"products\\/October2020\\/zmIOVAVtDz904jr8dzCd.jpg\"]', 15, 5, '2020-10-02 10:58:00', '2020-10-02 18:03:08', 'T011315'),
(82, 'SİYAH KUMAŞ KIRMIZI DESENLİ FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/V1GS7INbt6m1sG3exRaW.jpg', 6, 'si-yah-kumas-kirmizi-desenli-fi-yonk-toka', '[\"products\\/October2020\\/tCuZbzIka2zk1xFP9elb.jpg\",\"products\\/October2020\\/PZnlwWvLYqo1pvBbmNUR.jpg\"]', 15, 2, '2020-10-02 11:03:00', '2020-10-02 18:11:41', 'T011515'),
(83, 'PEMBE SATEN TAÇ TOKASI', 'ÇOCUKLAR İÇİN TASARLANMIŞ\r\nPEMBE SATEN \r\nÇOCUK \r\nTAÇ TOKASI', 'products/October2020/EiITSTRqA5nHWidAEMd9.jpg', 7, 'pembe-saten-tac-tokasi', '[\"products\\/October2020\\/LUE5Ha14H8MU0COBWhLZ.jpg\"]', 15, 1, '2020-10-02 11:09:10', '2020-10-02 11:09:10', 'TC010115'),
(84, 'PEMBE TÜL FİYONK', 'ÇOCUKLAR İÇİN TASARLANMIŞ\r\nGÜZEL BİR ÜRÜN', 'products/October2020/jNCDv76JWbOIDDHoic2U.jpg', 7, 'pembe-tul-fi-yonk', '[\"products\\/October2020\\/SIWEJRoK4CLNbsQY5SI9.jpg\"]', 15, 75, '2020-10-02 11:12:00', '2020-10-07 04:09:24', 'TC010215'),
(85, 'FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/4QQyt2hyUfrYGpA855Iv.jpg', 6, 'fi-yonk-toka', '[\"products\\/October2020\\/4MklW5EVpFfR0JHAF7J1.jpg\"]', 25, 2, '2020-10-03 05:24:44', '2020-10-03 05:24:44', 'T010225'),
(86, 'ANALI KIZLI PİTİ KARELİ', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/8ui0jrCfPyECjVbDf4xm.jpg', 6, 'anali-kizli-pi-ti-kareli', '[\"products\\/October2020\\/0L0C9tfORUzXKxucA99c.jpg\",\"products\\/October2020\\/ndZMWISXIilaMLlFRy2t.jpg\"]', 30, 0, '2020-10-03 05:27:00', '2020-10-03 05:28:29', 'T010730'),
(87, 'ANALI KIZLI KIRMIZI FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/IPX0dIWeYwgcCQXd6qVn.jpg', 6, 'anali-kizli-kirmizi-fi-yonk-toka', '[\"products\\/October2020\\/Z6FIFmLQQzwUPIHxRGsf.jpg\"]', 30, 3, '2020-10-03 05:52:42', '2020-10-03 05:52:42', 'T011630'),
(88, 'ANALI KIZLI MOR FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/RTaQ8oYnF5Sw3c8wiSXs.jpg', 6, 'anali-kizli-mor-fi-yonk-toka', '[\"products\\/October2020\\/YbMnKEWMm1UgXEdBOGUK.jpg\",\"products\\/October2020\\/3ETnZ3d9DJ98nHTGomYr.jpg\"]', 30, 4, '2020-10-03 05:54:00', '2020-10-04 10:07:32', 'T012530'),
(89, 'ANALI KIZLI FİYONK TOKA', '3 mekanizmalı otomatik tokalarla fiyonkları birleştirdik.						\r\nBirbirinden güzel modellerimizi özel ve gündelik kullanımınıza hazır hale getirdik.						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/hofS2LUFfT8APD9XkXjY.jpg', 6, 'anali-kizli-fi-yonk-toka', '[\"products\\/October2020\\/77vVNVQMfBPcuvi3HdQ2.jpg\",\"products\\/October2020\\/QvZueHu94QmxImW4rTVf.jpg\"]', 30, 2, '2020-10-05 07:26:00', '2020-10-05 07:27:41', 'T011230'),
(90, 'SATEN FİYON TOPUZ SAÇ TOKASI', '90\'ların modası topuz tokaları geri geliyor. Birbirinden şık otomatik topuz tokalarını sizler için tasarladık.						\r\nÖzel günlerinizde de gündelik hayatınızda da rahatlıkla kullanabileceğiniz bu tokalar sizi çok havalı gösterecek.						\r\nŞık olmanın en kolay yolu...						\r\nButik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/zCCADNuNuH7sE3iU5uJH.jpg', 6, 'saten-fi-yon-topuz-sac-tokasi', '[\"products\\/October2020\\/7K7S1rIctZSNzcxgOUSN.png\",\"products\\/October2020\\/49sPmCajoWpl9DGjuNXF.jpg\"]', 30, 5, '2020-10-05 10:17:00', '2020-10-06 05:09:00', 'T011930'),
(91, 'SİYAH TÜL ÇİÇEK TOPUZ SAÇ TOKASI', '90\'ların modası topuz tokaları geri geliyor. Birbirinden şık otomatik topuz tokalarını sizler için tasarladık.						\r\n	Özel günlerinizde de gündelik hayatınızda da rahatlıkla kullanabileceğiniz bu tokalar sizi çok havalı gösterecek.						\r\n	Şık olmanın en kolay yolu...						\r\n	Butik üretimdir, hepsinden sınırlı sayıda bulunur.', 'products/October2020/bcoAeNaOejM6glZlGNVT.jpg', 6, 'si-yah-tul-ci-cek-topuz-sac-tokasi', '[\"products\\/October2020\\/hepdWkdMvCHtbPlLOwSB.jpg\",\"products\\/October2020\\/imH7VBkwYvzcckcTjhM2.jpg\"]', 30, 5, '2020-10-07 11:33:00', '2020-10-07 11:34:30', 'T010125');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-09-16 06:06:01', '2020-09-16 06:06:01'),
(2, 'user', 'Normal User', '2020-09-16 06:08:36', '2020-09-16 06:08:36'),
(3, 'Site Operatörü', 'Site Operatörü', '2020-09-26 11:55:18', '2020-09-26 11:55:18'),
(4, 'Customers', 'Müşteriler', '2020-09-26 14:49:34', '2020-09-26 14:49:34');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Saltokasi.com', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'El yapımı', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Sal Tokasi Yonetim Paneli', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Yonetim Paneline Hos Geldiniz', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', 'UA-179398468-1', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `shopping_card`
--

CREATE TABLE `shopping_card` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `shopping_card_product`
--

CREATE TABLE `shopping_card_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sepet_id` bigint(20) UNSIGNED NOT NULL,
  `urun_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titleofimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `titleofimage`, `product_slug`, `created_at`, `updated_at`) VALUES
(1, 'sliders\\October2020\\rn1VujGptjxznVem4pYj.jpg', 'Son Moda Takımlar', 'anali-kizli-mor-fi-yonk-toka', '2020-10-07 21:40:23', '2020-10-07 21:40:23'),
(2, 'sliders\\October2020\\bIU9F7zDkfErGpeMExCy.jpg', 'Modaya yakın ürünler', NULL, '2020-10-07 21:42:16', '2020-10-07 21:42:16'),
(3, 'sliders\\October2020\\DaBDGs8lmpbnZsKlFWth.jpg', NULL, NULL, '2020-10-07 21:43:08', '2020-10-07 21:43:08');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-09-16 06:08:37', '2020-09-16 06:08:37'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-09-16 06:08:37', '2020-09-16 06:08:37');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT 4,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `surname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(11) DEFAULT 0,
  `phone_number` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `surname`, `is_active`, `phone_number`) VALUES
(1, 1, 'Umut', 'umut@email.com', 'users/default.png', NULL, '$2y$10$Wada3DCQ9tVcB2NJ/0BQN..MlV/VGzPGJA8rqWFTJaejrViU2zwSi', NULL, NULL, '2020-09-16 06:06:01', '2020-09-16 06:06:02', '', 0, 0),
(2, 3, 'Mustafa Günay', 'info@saltokasi.com', 'users/default.png', NULL, '$2y$10$y3GUMyfGFE331yxLKL.pROSYn9Eb8XVfIm9tX8e2LTyJZjr.XCz9y', 'rj0wNrmimhovdXjgtPbD2th5ZlU6bilVqNzdxuZ1D7IedjpuBxN8dctyc8ly', '{\"locale\":\"tr\"}', '2020-09-26 11:55:50', '2020-09-29 12:09:42', '', 0, 0),
(3, 4, 'customer1', 'customer1@gmail.com', 'users/default.png', NULL, '$2y$10$ss4PAm9KFz1NPFwa4fIGpeRPUMdPYGhwT/DLNhdn6Bo.uFxu4udci', NULL, '{\"locale\":\"tr\"}', '2020-09-26 14:50:09', '2020-09-26 14:50:09', '', 0, 0),
(33, 4, 'Umut Ozan Ozyildirim', 'ozanozy75@gmail.com', 'users/default.png', NULL, '$2y$10$TKaVnRNR.Ua2DgUtb5ZjXudAlYQJ5NXFo4uRxoGIWHSJZeZcN8Qju', 'xtb9wajT7saXtQuz7RBDtUsKMp4EIWxRiMfYqd547nFc9nLYF8sIJlMAn9lM', NULL, '2020-09-27 18:50:55', '2020-09-27 18:50:55', 'Ozyildirim', 0, 5386293141),
(35, 4, 'Umut Ozan Ozyildirim', 'testmettter@gmail.com', 'users/default.png', NULL, '$2y$10$hjjMOEpV7F0pdzQ1SwUSbebkAxOaFNmaE2oQUdelXjdRRQwDSMsMe', 'YcqiiXWmKVDOBpLjQ8xqpVg5qMLuo5hC91u4RMsuPML3rHUxshWZJTURszdV', NULL, '2020-09-27 18:52:40', '2020-09-27 18:52:40', 'test', 0, 5386293144),
(36, 4, 'Umut Ozan', 'ozy@stu.ayvansaray.edu.tr', 'users/default.png', NULL, '$2y$10$e1A9Kbckhe0rnl77EdxbC.JabSpDc.g834BzsReJWj7AIVPqAFo/a', 'aIN8chjVlM9BcLTZxZwuZxICq0hBu8PwehLZ3Ul2PErez0TYbunzMFfApdWM', NULL, '2020-09-27 18:53:08', '2020-09-27 18:53:08', 'Ozyildirim', 0, 5386293151),
(37, 4, 'Umut Ozan Ozyildirim', 'jasonblaclo@gmail.com', 'users/default.png', NULL, '$2y$10$iWRlLRaUWGpM9jUmPf2LfuIVmh/3lBQzUCXyGGGiBgsyoTmCfr32y', 'G9cFzPiBb7toUejDShnyuWpBGBPn8T6kgCxgB7aaFe5VUDu6dh4hps7pLrsw', NULL, '2020-09-27 19:04:45', '2020-09-27 19:04:45', 'Ozyildirim', 0, 5386293124),
(38, 4, 'Umut Ozan Ozyildirim', 'ozanozy@gmail.com', 'users/default.png', NULL, '$2y$10$xqTnnfSf2xT5wdwhU5Tele2wLkyvMJsBGvqmMJhY1OTjQsLk1qcdC', 'ynBKmbUs2LZ6m5MWNlG1BYDlfd69B8Wa4kppsVMADSabJuYJ56Kfm8W0wubP', NULL, '2020-09-27 19:06:36', '2020-09-27 19:06:36', 'Ozyildirim', 0, 5386293166),
(39, 4, 'Umut Ozan Ozyildirim', 'jasonstatham@gmail.com', 'users/default.png', NULL, '$2y$10$FJd.z/4jkJinh/i6HfvGgOJoAf1/OAU8JaqOYaHiSugZBqh1SC6We', '48tshKnfpnoWga2lWWpRjz2VlXrguTp0VGx3yck3uVP1dwkSWxWx2a5FoAOu', NULL, '2020-09-29 12:27:48', '2020-09-29 12:27:48', 'Ozyildirim', 0, 5386293156),
(40, 4, 'Umut Ozan Ozyildirim', 'umutozn@gmail.com', 'users/default.png', NULL, '$2y$10$a2NjQnloTeWrU/SZQUO0.OITmRB0d9Xw1qkGIHNoeACTUy.J1vaAG', 'TkNKf6YK6jANyRbG9GNq0OAGUO2iIAE0A1FiOvknE5MS3d5w8YdWXW9kagq3', NULL, '2020-09-29 12:29:20', '2020-09-29 12:29:20', 'test', 0, 5386293176),
(41, 4, 'Umut Ozan Ozyildirim', 'ozanozy75@outlook.com', 'users/default.png', NULL, '$2y$10$mxElzzPE5e6x3W2me4.qa.Qaje6FA8U.gxxkDeLZF0knTtdEC5DNu', 'GoH5YbKJnb0McXfrAnfo8OhBe5kBtAvr8zUpk5GOh8KmleyZkd1MVaAnn4W3', NULL, '2020-09-29 12:31:06', '2020-09-29 12:31:06', 'Test', 0, 5386295678);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Tablo için indeksler `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Tablo için indeksler `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `featured_images`
--
ALTER TABLE `featured_images`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `frontend_users`
--
ALTER TABLE `frontend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frontendusers_email_unique` (`email`);

--
-- Tablo için indeksler `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Tablo için indeksler `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_sepet_id_foreign` (`sepet_id`);

--
-- Tablo için indeksler `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Tablo için indeksler `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Tablo için indeksler `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Tablo için indeksler `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Tablo için indeksler `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Tablo için indeksler `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Tablo için indeksler `shopping_card`
--
ALTER TABLE `shopping_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shopping_card_user_id_foreign` (`user_id`);

--
-- Tablo için indeksler `shopping_card_product`
--
ALTER TABLE `shopping_card_product`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Tablo için indeksler `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Tablo için AUTO_INCREMENT değeri `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- Tablo için AUTO_INCREMENT değeri `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `featured_images`
--
ALTER TABLE `featured_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `frontend_users`
--
ALTER TABLE `frontend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- Tablo için AUTO_INCREMENT değeri `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Tablo için AUTO_INCREMENT değeri `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Tablo için AUTO_INCREMENT değeri `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- Tablo için AUTO_INCREMENT değeri `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Tablo için AUTO_INCREMENT değeri `shopping_card`
--
ALTER TABLE `shopping_card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `shopping_card_product`
--
ALTER TABLE `shopping_card_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_sepet_id_foreign` FOREIGN KEY (`sepet_id`) REFERENCES `shopping_card_product` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `shopping_card`
--
ALTER TABLE `shopping_card`
  ADD CONSTRAINT `shopping_card_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Tablo kısıtlamaları `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Tablo kısıtlamaları `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
